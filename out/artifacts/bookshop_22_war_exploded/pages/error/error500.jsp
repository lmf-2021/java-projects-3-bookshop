<%--
  Created by IntelliJ IDEA.
  User: 24044
  Date: 2021/6/14
  Time: 22:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>出错友好页面</title>
    <%@ include file="/pages/common/head.jsp"%>
</head>
<body>
很抱歉，您访问的后台程序出了问题。程序员小哥，正在努力为您抢修中......<br/>
<a href="index.jsp">返回首页</a>
</body>
</html>
