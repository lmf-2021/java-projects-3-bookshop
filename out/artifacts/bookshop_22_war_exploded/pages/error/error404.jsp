<%--
  Created by IntelliJ IDEA.
  User: 24044
  Date: 2021/6/14
  Time: 22:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>出错友好页面</title>
    <%@ include file="/pages/common/head.jsp"%>
</head>
<body>
很抱歉！您访问的页面不存在，或您访问的页面已被删除！<br/>
<a href="index.jsp">返回首页</a>
</body>
</html>
