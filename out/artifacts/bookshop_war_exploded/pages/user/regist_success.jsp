<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>尚硅谷书城会员注册页面</title>
<%--	<!--	base标签中最后的“斜杠”不能去掉，否则表示的意义就不一样，路径一般写到工程路径-->--%>
<%--	<base href="http://localhost:8080/bookshop_22/">--%>
<%--<link type="text/css" rel="stylesheet" href="static/css/style.css" >--%>

	<%--	静态包含，base标签，css样式文件，jQuery文件--%>
	<%@ include file="/pages/common/head.jsp"%>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
	
	h1 a {
		color:red;
	}
</style>
</head>
<body>
		<div id="header">
				<img class="logo_img" alt="" src="static/img/logo.gif" >
				<span class="wel_word"></span>
			<%--			静态包含，登录成功--%>
			<%@ include file="/pages/common/login_success_menu.jsp"%>
		</div>
		
		<div id="main">
		
			<h1>注册成功! <a href="../../index.jsp">转到主页</a></h1>
	
		</div>

		<%--	静态包含页脚内容--%>
		<%@include file="/pages/common/footer.jsp"%>
</body>
</html>