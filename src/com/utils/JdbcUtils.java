package com.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcUtils {

    private static DruidDataSource dataSource;
    private static ThreadLocal<Connection> conns = new ThreadLocal<Connection>();
    static {
        try {
            //创建一个资源配置文件对象
            Properties pros = new Properties();
            // 读取 jdbc2.properties 属性配置文件，并返回一个数据库连接
            InputStream is = JdbcUtils.class.getClassLoader().getResourceAsStream("jdbc.properties");
//            InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("jdbc.properties");

            // 从流中加载数据
            pros.load(is);
            // 创建数据库连接池
            dataSource =(DruidDataSource) DruidDataSourceFactory.createDataSource(pros);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        Connection conn = JdbcUtils.getConnection();
//        System.out.println(conn);
//    }

    /**
     * 获取数据库连接池中的连接
     * @return 如果返回 null,说明获取连接失败<br/>有值就是获取连接成功
     */
    //方式一：没有使用TreadLocal
//    public static Connection getConnection(){
//        Connection conn = null;
//        try {
//            conn = dataSource.getConnection();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return conn;
//    }

    public static Connection getConnection(){
        Connection conn=conns.get();//从TreadLocal域中，获取所保存的数据库链接
        if (conn == null) {
            try {
                conn=dataSource.getConnection();//从数据库连接池中获取连接
                conns.set(conn);//将conn链接，保存到TreadLocal中，供后面JDBC操作使用
                conn.setAutoCommit(false);//将事务设置为手动提交模式
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return conn;
    }

    /**
     *     提交事务，并关闭连接
     */
    public static void commitAndClose(){
        Connection connection = conns.get();
        if (connection != null) {// 如果不等于 null，说明 之前使用过连接，操作过数据库
            try {
                connection.commit(); // 提交 事务
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.close(); // 关闭连接，资源资源
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        //一定要执行 remove 操作，否则就会出错。（因为 Tomcat 服务器底层使用了线程池技术）
        conns.remove();
    }

    /**
     *     回滚事务，并关闭连接
     */
    public static void rollbackAndClose(){
        Connection connection = conns.get();
        if (connection != null) {// 如果不等于 null，说明 之前使用过连接，操作过数据库
            try {
                connection.rollback(); // 回滚 事务
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.close(); // 关闭连接，资源资源
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        //一定要执行 remove 操作，否则就会出错。（因为 Tomcat 服务器底层使用了线程池技术）
        conns.remove();
    }

    /***
     * 关闭连接，放回数据库连接池
     * @param conn
     */
    //方式一：
//    public static void close(Connection conn){
//        if (conn != null) {
//            try {
//                conn.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
