package com.test;

import com.utils.JdbcUtils;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcUtilsTest {
    /**
     * 测试从数据库连接池中，获取数据库连接
     * @throws SQLException
     */
    @Test
    public void show() throws SQLException {
            //获取连接
            Connection connection = JdbcUtils.getConnection();
        String sql = "select * from t_user  where id < ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, 30);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String usernaem = resultSet.getString(2);
            System.out.println("###" + usernaem);
        }
        //打印连接
            System.out.println(connection);
            //关闭连接，将连接返回数据库连接池
            JdbcUtils.commitAndClose();

    }


}
