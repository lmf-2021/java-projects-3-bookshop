package com.test;

import com.dao.BookDao;
import com.dao.impl.BookDaoImpl;
import com.pojo.Book;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class BookDaoTest {
    BookDao bookDao=new BookDaoImpl();
    @Test
    public void addBook() {
        bookDao.addBook(new Book(null,"书籍1", "作者1", new BigDecimal(9999),1100000,0,null ));
    }

    @Test
    public void deleteBookById() {
        bookDao.deleteBookById(21);
    }

    @Test
    public void updateBook() {
        bookDao.updateBook(new Book(23,"大家帅才是真的帅！", "xclmf", new BigDecimal(9999),1100000,0,null ));
    }

    @Test
    public void queryBookById() {
        System.out.println( bookDao.queryBookById(22) );
    }

    @Test
    public void queryBooks() {
        for (Book queryBook : bookDao.queryBooks()) {
            System.out.println(queryBook);
        }
    }
    @Test
    public void queryForPageTotalCount() {
        System.out.println(bookDao.queryForPageTotalCount());

    }
    @Test
    public void queryForPageTotalCountByPrice() {
        System.out.println(bookDao.queryForPageTotalCountByPrice(10,50));

    }

    @Test
    public void queryForPageItems() {
        for (Book book : bookDao.queryForPageItems(0,4)) {
            System.out.println(book);
        }

    }
    @Test
    public void queryForPageItemsByPrice() {
        for (Book book : bookDao.queryForPageItemsByPrice(0,6,10,50)) {
            System.out.println(book);
        }

    }
}