package com.test;

import com.pojo.User;
import com.service.UserService;
import com.service.impl.UserServiceImpl;
import org.junit.Test;

public class UserServiceTest {
    UserService userService=new UserServiceImpl();

    @Test
    public void registUser() {
        userService.registUser(new User(null,"lmf16811","552200","lmf168@qq.com"));
        userService.registUser(new User(null,"xc16811","552200","xc168@qq.com"));
    }

    @Test
    public void login() {

        System.out.println(userService.login(new User(null,"xc16811","552200","null")));
    }

    @Test
    public void existsUsername() {
        if (userService.existsUsername("xc168")) {
            System.out.println("用户名已存在");
        }else{
            System.out.println("用户名可用");
        }
    }
}