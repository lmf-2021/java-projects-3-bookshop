package com.test;

import com.dao.UserDao;
import com.dao.impl.UserDaoImpl;
import com.pojo.User;
import org.junit.Test;

/**
 * 测试UserDao各种方法是否有效
 */
public class UserDaoTest {
    //创建一个UserDao的实现类对象（这里用的多态）
    UserDao userDao = new UserDaoImpl();

    /**
     * 查询用户名是否存在
     */
    @Test
    public void queryUserByUsername() {
        if (userDao.queryUserByUsername("admin") == null ){
            System.out.println("用户名可用！");
        } else {
            System.out.println("用户名已存在！");
        }
    }

    /**
     * 查询用户名和密码是否存在
     */
    @Test
    public void queryUserByUsernameAndPassword() {
        if ( userDao.queryUserByUsernameAndPassword("admin","admin") == null) {
            System.out.println("用户名或密码错误，登录失败");
        } else {
            System.out.println("查询成功");
        }
    }

    /**
     * 保存用户账号信息
     */
    @Test
    public void saveUser() {
        //注意：因为数据库表创建的时候，添加约束了。所以用户名与密码不能重复。
        System.out.println( userDao.saveUser(new User(null,"aaaa", "552200", "aaaa1688@qq.com")) );
    }
}