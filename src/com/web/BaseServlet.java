package com.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public abstract class BaseServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //解决请求中文乱码
        req.setCharacterEncoding("UTF-8");
        ////统一浏览器的字符集编码格式：utf-8,解决响应中文乱码
        resp.setContentType("text/html;charset=UTF-8");
        //获取隐藏域的值
        String action = req.getParameter("action");
//        //根据隐藏域的值来判断处理登录还是注册业务
//        if ("login".equals(action)) {
//            //处理登录事务
//            login(req,resp);
//        }else if("regist".equals(action)){
//            //处理注册业务
//            regist(req,resp);
//        }

        try {
        // 获取 action 业务鉴别字符串，获取相应的业务 方法反射对象
            Method method = this.getClass().getDeclaredMethod(action, HttpServletRequest.class, HttpServletResponse.class);
//            System.out.println(method);
        // 调用目标业务 方法
            method.invoke(this, req, resp);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();//将异常抛到filter监视器中去
        }
    }
}
