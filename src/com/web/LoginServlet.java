package com.web;

import com.pojo.User;
import com.service.UserService;
import com.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取请求参数
        String username = req.getParameter("username");//获取用户名
        String password = req.getParameter("password");//获取用户密码
        //2.调用 userService.login()登录处理业务
        UserService userService=new UserServiceImpl();
        User loginUser = userService.login(new User(null, username, password, null));
        if (loginUser==null) {

        //3.如果等于 null,说明登录 失败!
            System.out.println("用户名或密码错误，登录失败");
            //把错误信息和回显的表单项信息。保存到域对象中
            req.setAttribute("msg","用户名或密码错误");
            req.setAttribute("username",username);
            //返回登录页面
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req,resp);
        } else {
        //4.如果不等于null，说明登录成功，跳转到登录成功页面
            System.out.println("登录成功");
            req.getRequestDispatcher("/pages/user/login_success.jsp").forward(req,resp);

        }
    }
}
