package com.service;

import com.pojo.User;

public interface UserService {
    /**
     * 注册用户
     * @param user
     */
    public void registUser(User user);

    /**
     * 登录用户
     * @param user
     * @return 如果返回null，则登陆失败，如果返回有值，则登录成功
     */
    public User login(User user);

    /**
     * 检查用户名是否存在
     * @param username
     * @return
     */
    public boolean existsUsername(String username);
}
