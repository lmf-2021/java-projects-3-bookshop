<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>尚硅谷书城会员注册页面</title>

<%--<!--	base标签中最后的“斜杠”不能去掉，否则表示的意义就不一样，路径一般写到工程路径-->--%>
<%--	<base href="http://localhost:8080/bookshop_22/">--%>

<%--<!--	使用相对路径加载css样式文件，但是其他地方的路径没有更改，所以直接运行会出问题-->--%>
<%--<!--	<link type="text/css" rel="stylesheet" href="../../static/css/style.css" >-->--%>

<%--<!--	使用绝对路径加载css样式文件-->--%>
<%--	<link type="text/css" rel="stylesheet" href="static/css/style.css" >--%>
<%--	<script type="text/javascript" src="static/script/jquery-1.7.2.js"></script>--%>


<%--	静态包含，base标签，css样式文件，jQuery文件--%>
	<%@ include file="/pages/common/head.jsp"%>
	<script type="text/javascript" >
		//绑定页面加载完成事件
		$(function () {
			//给“username”绑定失去焦点事件
			$("#username").blur(function () {
				//获取用户名
				var usernameValue = this.value;
				$.getJSON("http://localhost:8080/bookshop_22/userServlet","action=ajaxExistsUsername&username="+usernameValue,function (data) {
					console.log(data);
					if (data.existsUsername) {
						$(".errorMsg").html("用户名已存在"); 			// result 等于 1，说明用户名存在
					} else {
						$(".errorMsg").html("用户名可用");
					}
				})
			})

			//给验证码图片绑上单击事件
			$("#code_img").click(function () {
				// 在事件响应的 function 函数中有一个 this 对象。这个 this 对象，是当前正在响应事件的 dom 对象
				// src 属性表示验证码 img 标签的 图片路径。它可读，可写
				// alert(this.src);
				this.src="${basePath}kaptcha.jpg?d="+new Date();
			})

			//给注册绑定单击事件
			$("#sub_btn").click(function () {


				/**
				 * 验证用户名：必须由字母，数字下划线组成，并且长度为 5 到 12 位
				 */
				// 1 获取用户名输入框里的内容
				var usernameText = $("#username").val();
				//alert(usernameText)
				// 2 创建正则表达式对象
				var usernamePatt=/^\w{5,12}$/;
				//alert(usernamePatt);
				// 3 使用 test 方法验证
				if (!usernamePatt.test(usernameText)){
				// 4 提示用户结果
					$("span.errorMsg").text("用户名不合法");
					return false;
				}


				/**
				 * 验证用户密码：必须由字母，数字下划线组成，并且长度为 5 到 12 位
				 */
				// 1 获取用户密码输入框里的内容
				var passwordText = $("#password").val();
				//alert(usernameText)
				// 2 创建正则表达式对象
				var passwordPatt=/^\w{5,12}$/;
				//alert(usernamePatt);
				// 3 使用 test 方法验证
				if (!passwordPatt.test(passwordText)){
					// 4 提示用户结果
					$("span.errorMsg").text("密码不合法");
					return false;
				}


				/**
				 * 验证确认密码：必须由字母，数字下划线组成，并且长度为 5 到 12 位
				 */

				//1 获取确认密码内容
				var repwdText = $("#repwd").val();
				// 2 和密码相比较
				if (repwdText != passwordText) {
					//3 提示用户
					$("span.errorMsg").text("验证密码与密码不一致")
					return false;
				}

				/**
				 * 验证邮箱：xxxxx@xxx.com
				 */
				//1.获取邮箱内容
				let emailText = $("#email").val();
				//2 创建正则表达式对象
				var emailPatt = /^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/;
				//3 使用 test 方法验证是否合法
				if(!emailPatt.test(emailText)){
					//4 提示用户
					$("span.errorMsg").text("邮箱格式不正确")
					return false;
				}

				/**
				 * 验证码：现在只需要验证用户已输入。因为还没讲到服务器。验证码生成。
				 */
				//1.获取验证码内容
				var codeText=$("#code").val();
					//去掉字符串两端地空格
				codeText=$.trim(codeText);
				//2.判断内容是否为空
				if(codeText == null || codeText == ""){
					//3.提示用户
					$("span.errorMsg").text("验证码内容不能为空")
					return false;
				}

				$("span.errorMsg").text("");//合法就不提示

			})

		})
	</script>
<style type="text/css">
	.login_form{
		height:420px;
		margin-top: 25px;
	}

</style>
</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
		</div>
		
			<div class="login_banner">
			
				<div id="l_content">
					<span class="login_word">欢迎注册</span>
				</div>
				
				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>注册书城会员</h1>
								<span class="errorMsg">
<%--									<%=request.getAttribute("msg")==null?"":request.getAttribute("msg")%>--%>
									${requestScope.msg}
								</span>
							</div>
							<div class="form">
								<form action="userServlet" method="post">
                                    <input type="hidden" name="action" value="regist"/>
                                    <label>用户名称：</label>
									<input class="itxt" type="text" placeholder="请输入用户名"
										   autocomplete="off" tabindex="1" name="username" id="username"
									value="${requestScope.username}"/>
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt" type="password" placeholder="请输入密码"
										   autocomplete="off" tabindex="1" name="password" id="password" />
									<br />
									<br />
									<label>确认密码：</label>
									<input class="itxt" type="password" placeholder="确认密码"
										   autocomplete="off" tabindex="1" name="repwd" id="repwd" />
									<br />
									<br />
									<label>电子邮件：</label>
									<input class="itxt" type="text" placeholder="请输入邮箱地址"
										   autocomplete="off" tabindex="1" name="email" id="email"
										   value="${requestScope.email}"/>
									<br />
									<br />
									<label>验证码：</label>
									<input class="itxt" type="text" style="width: 150px;" name="code" id="code"/>
									<img id="code_img" alt="" src="kaptcha.jpg" style="float: right; margin-right: 40px; width: 80px;height: 40px">
									<br />
									<br />
									<input type="submit" value="注册" id="sub_btn" />
									
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		<%--	静态包含页脚内容--%>
		<%@include file="/pages/common/footer.jsp"%>
</body>
</html>